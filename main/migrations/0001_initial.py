# Generated by Django 4.1 on 2022-08-19 17:33

from django.db import migrations, models
import django.db.models.deletion
import main.models
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ClientModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('phoneNumber', phonenumber_field.modelfields.PhoneNumberField(max_length=128, region='RU', unique=True, verbose_name='Телефонный номер клиента')),
                ('mobileOperatorCode', main.models.IntegerRangeField(verbose_name='Код мобильного оператора')),
                ('tag', models.CharField(max_length=20, verbose_name='Тег клиента')),
                ('timeZone', main.models.IntegerRangeField(verbose_name='Часовой пояс клиента')),
            ],
            options={
                'verbose_name': 'Клиенты',
            },
        ),
        migrations.CreateModel(
            name='MailingListModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mailingStartTime', models.DateTimeField(verbose_name='Дата и время запуска рассылки')),
                ('text', models.CharField(max_length=70, verbose_name='Текст сообщения рассылки')),
                ('mailingEndTime', models.DateTimeField(verbose_name='Дата и время окончания рассылки')),
                ('clientFilter', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.clientmodel', verbose_name='Фильтр свойств клиентов')),
            ],
            options={
                'verbose_name': 'Рассылки',
            },
        ),
        migrations.CreateModel(
            name='MessageModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('createDate', models.DateTimeField(verbose_name='Дата и время создания сообщения')),
                ('sendingStatus', main.models.IntegerRangeField(verbose_name='Статус отправки')),
                ('clientId', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.clientmodel', verbose_name='ID клиента, которому отправили сообщение')),
                ('mailingId', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.mailinglistmodel', verbose_name='ID рассылки, в которой было отправлено сообщение')),
            ],
            options={
                'verbose_name': 'Сообщения',
            },
        ),
    ]
