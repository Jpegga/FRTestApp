from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class IntegerRangeField(models.IntegerField):
    def __init__(self, verbose_name=None, name=None, min_value=None, max_value=None, **kwargs):
        self.min_value, self.max_value = min_value, max_value
        models.IntegerField.__init__(self, verbose_name, name, **kwargs)

    def formfield(self, **kwargs):
        defaults = {'min_value': self.min_value, 'max_value':self.max_value}
        defaults.update(kwargs)
        return super(IntegerRangeField, self).formfield(**defaults)


class ClientModel(models.Model):
    phoneNumber = PhoneNumberField(null=False, blank=False, unique=True, region='RU', verbose_name='Телефонный номер клиента')
    mobileOperatorCode = IntegerRangeField(min_value=900, max_value=999, verbose_name='Код мобильного оператора')
    tag = models.CharField(max_length=20, verbose_name='Тег клиента')
    timeZone = IntegerRangeField(min_value=-12, max_value=12, verbose_name='Часовой пояс клиента')

    class Meta:
        verbose_name = 'Клиенты'

    def __str__(self):
        return self.phoneNumber


class MailingListModel(models.Model):
    mailingStartTime = models.DateTimeField(verbose_name='Дата и время запуска рассылки')
    text = models.CharField(max_length=70, verbose_name='Текст сообщения рассылки')
    clientFilter = models.ForeignKey(ClientModel, on_delete=models.CASCADE, verbose_name='Фильтр свойств клиентов')
    mailingEndTime = models.DateTimeField(verbose_name='Дата и время окончания рассылки')

    class Meta:
        verbose_name = 'Рассылки'

    def __str__(self):
        return self.mailingStartTime


class MessageModel(models.Model):
    createDate = models.DateTimeField(verbose_name='Дата и время создания сообщения')
    sendingStatus = IntegerRangeField(min_value=100, max_value=599, verbose_name='Статус отправки')
    mailingId = models.ForeignKey(MailingListModel, on_delete=models.CASCADE, verbose_name='ID рассылки, в которой было отправлено сообщение')
    clientId = models.ForeignKey(ClientModel, on_delete=models.CASCADE, verbose_name='ID клиента, которому отправили сообщение')

    class Meta:
        verbose_name = 'Сообщения'

    def __str__(self):
        return self.createDate
